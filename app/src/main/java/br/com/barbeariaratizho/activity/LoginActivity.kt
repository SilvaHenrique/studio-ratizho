package br.com.barbeariaratizho.activity

import android.content.Intent
import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.Window
import android.widget.*
import br.com.barbeariaratizho.R
import br.com.barbeariaratizho.helper.ConfiguracaoFirebase
import br.com.barbeariaratizho.model.Usuario
import com.google.android.gms.tasks.OnCompleteListener

class LoginActivity : AppCompatActivity()  {



    lateinit var campoEmail: EditText
    lateinit var campoSenha: EditText
    lateinit var botaoEntrar: Button
    lateinit var progressBar: ProgressBar


    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        this.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

        verificarUsuarioLogado()
        inicializarComponentes()

        botaoEntrar.setOnClickListener(View.OnClickListener {
            if (!this@LoginActivity.validateFields()) {
                return@OnClickListener
            }
            fazerLogin()
        })
        val cadastroActivity: TextView = findViewById(R.id.textCadastrar)
        cadastroActivity.setOnClickListener {
            val intent = Intent(this, CadastroActivity::class.java)
            startActivity(intent)
        }
    }

    private fun validateFields(): Boolean {
        val textoEmail: String = campoEmail.text.toString()
        val textoSenha: String = campoSenha.text.toString()

        if (textoEmail.isEmpty()) {
            Toast.makeText(this, "Preencha a o e-mail!", Toast.LENGTH_SHORT).show()
            return false
        }

        if (textoSenha.isEmpty()) {
            Toast.makeText(this, "Preencha a senha!", Toast.LENGTH_SHORT).show()
            return false
        }
        return true
    }

    private fun verificarUsuarioLogado(){
        val autentica = ConfiguracaoFirebase.getFirenciaAutenticacao()
        if(autentica.currentUser != null){
            startActivity(Intent(applicationContext, MainActivity::class.java))
            finish()
        }
    }

    private fun fazerLogin(){
        progressBar.visibility = View.VISIBLE
        val autenticacao = ConfiguracaoFirebase.getFirenciaAutenticacao()
        autenticacao.signInWithEmailAndPassword(
            campoEmail.text.toString(),
            campoSenha.text.toString()
        ).addOnCompleteListener(OnCompleteListener {
        task -> if (task.isSuccessful){
            progressBar.visibility = View.GONE
            startActivity(Intent(applicationContext, MainActivity::class.java))
            Toast.makeText(this, "Entrando!", Toast.LENGTH_SHORT).show()
            finish()
        }else{
            Toast.makeText(this, "Erro ao fazer login!", Toast.LENGTH_SHORT).show()
            progressBar.visibility = View.GONE
        }
        })
    }


    fun inicializarComponentes(){

        campoEmail = findViewById(R.id.editLoginEmail)
        campoSenha = findViewById(R.id.editLoginSenha)
        botaoEntrar = findViewById(R.id.buttonLogin)
        progressBar = findViewById(R.id.progressLogin)

        campoEmail.requestFocus()
    }

}
