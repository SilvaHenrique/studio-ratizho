package br.com.barbeariaratizho.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import br.com.barbeariaratizho.R
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        barbeariaButton.setOnClickListener{
            startActivity(Intent(this, CabeleireiroActivity::class.java))
            Toast.makeText(this, "BARBEARIA!", Toast.LENGTH_LONG).show()
        }

        GrafitButton.setOnClickListener{
            startActivity(Intent(this, GraftActivity::class.java))
            Toast.makeText(this, "Trabalho com Fachadas!", Toast.LENGTH_LONG).show()
        }
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when(item.itemId){
            R.id.menu_sair -> {
                FirebaseAuth.getInstance().signOut()
                startActivity(Intent(applicationContext, LoginActivity::class.java))
                Toast.makeText(this, "Saindo", Toast.LENGTH_LONG).show()
            }
        }

        return super.onOptionsItemSelected(item)
    }




}
