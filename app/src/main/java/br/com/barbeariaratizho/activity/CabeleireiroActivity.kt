package br.com.barbeariaratizho.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import br.com.barbeariaratizho.MapsActivity
import br.com.barbeariaratizho.R
import br.com.barbeariaratizho.fragment.ContatoFragment
import br.com.barbeariaratizho.fragment.MenuGaleriaFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.bottom_navigation.*

class CabeleireiroActivity : AppCompatActivity() {

    private val opcoesNavegacao = BottomNavigationView.OnNavigationItemSelectedListener { item->
        when(item.itemId){
            R.id.ic_home-> {
                print("Menu")
                subistituirFragmento(MenuGaleriaFragment())
                return@OnNavigationItemSelectedListener true
            }R.id.ic_contact-> {
                print("Contato")
                subistituirFragmento(ContatoFragment())
                return@OnNavigationItemSelectedListener true
            }R.id.ic_locomotion-> {
                print("Localização")
                startActivity(Intent(this, MapsActivity::class.java))
                return@OnNavigationItemSelectedListener true
            }R.id.ic_profile-> {
                print("Perfil")
                return@OnNavigationItemSelectedListener true
            }

        }
        false
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cabeleireiro)

        bottomNavigation.setOnNavigationItemSelectedListener(opcoesNavegacao)
        subistituirFragmento(MenuGaleriaFragment())

    }


    private fun subistituirFragmento(fragment: Fragment){
        // supportFragmenteManager para conseguir um FragmentManager em seguida usar beginTransaction e replace para trocar um fragment
        val trasacoesFragment = supportFragmentManager.beginTransaction()
        trasacoesFragment.replace(R.id.fragmentContainer, fragment).commit()

    }
}
