package br.com.barbeariaratizho.activity



import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.text.TextUtils
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import br.com.barbeariaratizho.R
import br.com.barbeariaratizho.helper.ConfiguracaoFirebase
import br.com.barbeariaratizho.model.Usuario
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_cadastro.*
import kotlin.math.log

class CadastroActivity : AppCompatActivity() {

    //Elementos da interface do usuario
    private lateinit var etName: EditText
    private lateinit var etEmail: EditText
    private lateinit var etPassword: EditText

    private lateinit var dbReference: DatabaseReference
    private lateinit var database: FirebaseDatabase
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cadastro)



        etName = findViewById(R.id.editCadastrarNome)
        etEmail = findViewById(R.id.editCadastrarEmail)
        etPassword = findViewById(R.id.editCadastrarSenha)



        database = FirebaseDatabase.getInstance()
        auth = FirebaseAuth.getInstance()

        dbReference = database.reference.child("User")

    }

    override fun onResume() {
        super.onResume()
        buttonCadastro.setOnClickListener { createNewAccount() }
    }

    private fun createNewAccount(){
        val name = etName.text.toString()
        val email = etEmail.text.toString()
        val password = etPassword.text.toString()
        if(name.isNotEmpty() && email.isNotEmpty() && password.isNotEmpty()){
            progressBar.visibility = View.VISIBLE
            auth.createUserWithEmailAndPassword(email, password).addOnSuccessListener { task ->
                val userId = auth.currentUser!!.uid
                verifyEmail()

                val userBD=dbReference.child(userId)
                userBD.child("name").setValue(name)
                action()
            }.addOnFailureListener { failure ->
                failure.printStackTrace()
            }
        }
    }

    private fun action(){
        startActivity(Intent(this, LoginActivity::class.java))
    }

    private fun verifyEmail(){
        val user = auth.currentUser
        user!!.sendEmailVerification()
            .addOnCompleteListener(this){
                task ->

                if(task.isComplete){
                    Toast.makeText(this, "Email enviado " + user.email, Toast.LENGTH_SHORT).show()
                }else{
                    Toast.makeText(this, "Erro ao enviar email", Toast.LENGTH_SHORT).show()
                }
            }
    }


}
