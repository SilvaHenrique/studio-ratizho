package br.com.barbeariaratizho

import android.location.LocationProvider
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.core.graphics.createBitmap
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_maps.*

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onResume() {
        super.onResume()
        val errorCode = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this)
        when(errorCode){
            1 -> ConnectionResult.SERVICE_MISSING
            2 -> ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED
            3 -> ConnectionResult.SERVICE_DISABLED

            4 -> ConnectionResult.RESULT_SUCCESS

        }


    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        val icon = BitmapDescriptorFactory.fromResource(R.drawable.barbearia_chegar)

        // Add a marker in Sydney and move the camera
        val studioRatinho = LatLng(-23.702800, -46.801887)
        mMap.addMarker(MarkerOptions().position(studioRatinho).title("Ratizho cabeleireiro").icon(
            icon
        ))
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(studioRatinho, 15f))
    }


}
