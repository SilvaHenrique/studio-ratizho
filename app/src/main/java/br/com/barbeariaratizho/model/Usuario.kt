package br.com.barbeariaratizho.model

data class Usuario(
    var id: String? = null,
    var nome: String? = null,
    var email: String? = null,
    var senha: String? = null,
    var caminhoFoto: String? = null
)